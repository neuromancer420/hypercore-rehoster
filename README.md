# Hypercore Rehoster

Help host the hypercores of your choice.

Warning: still in alfa

For a hypercore containing a hyperdrive, both underlying cores will be served.

If you rehost another rehoster, you will rehost all its cores as well (recursively).


See [hypercore-rehost-server](https://gitlab.com/HDegroote/hypercore-rehost-server) for a server wrapping the rehoster, and [hypercore-rehost-cli](https://gitlab.com/HDegroote/hypercore-rehost-cli) for a CLI to interact with that server.

## Description

The rehoster automatically keeps the cores it hosts up to date, by continuously listening for changes in the background.

The rehoster is recursive: if you rehost the key of another rehoster, you will automatically
also rehost all the cores contained in that rehoster (and if those also contain rehoster keys, you will host their cores as well, recursively).

Rehosters can host one other, in which case they will host the union of all their individual cores.

A Rehoster uses a corestore to store the hypercores on disk.

The db (hyperbee) containing the hosted hypercores is also stored in the corestore.

## Install

`npm i hypercore-rehoster`

## API

### `const rehoster = new Rehoster(corestore, { bee, beeName, swarm})`
Initialises a rehoster.

if no swarm is specified, a new one with default options will be created.

If a bee is passed, that bee will be used as database.
If not, the bee will be opened from the corestore, based on the beeName (or the default name if none is provided).

Note when passing a bee: to be able to add/delete cores, the corestore should have write rights on the bee (it should have been created with that corestore).

### `await rehoster.ready()`
Set up the the rehoster, so it starts downloading and serving all the keys it contains.
### `await rehoster.add(key)`
Add a new key to be rehosted (any valid key format can be used).

Note that connecting with other peers and downloading the core's content happens in the background, so not all content is immediately rehosted after `add` finishes.

### `await rehoster.delete(key)`
Remove a key.

Note that propagating the delete to recursively hosted cores happens in the background
(so a sub-rehoster's cores will not yet be unhosted immediately after `delete` finishes)

### `await rehoster.close()`
Close the rehoster, and clean up.

## Usage

See [example.js](example.js).
