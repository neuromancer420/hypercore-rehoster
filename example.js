const Corestore = require('corestore')
const Rehoster = require('./index.js')
const ram = require('random-access-memory')

const corestoreLoc = ram // './my-store' for persistence on the specified file

async function main () {
  const corestore = new Corestore(corestoreLoc)

  const rehoster = new Rehoster(corestore)

  const someCore = corestore.get({ name: 'mycore' })
  await someCore.ready()
  await rehoster.add(someCore.key)

  console.log('rehoster served discovery keys:')
  console.log(rehoster.servedDiscoveryKeys)
  // Note: a rehoster always serves itself, so will log 2 keys

  console.log('\nIf you add the key of another rehoster, then it will recursively serve all its works')

  const corestore2 = new Corestore(ram)

  const rerehoster = new Rehoster(corestore2)

  // This ensures the other rehoster already flushed its topics to the swarm
  // In practice you don't need to worry about this, it just helps solve a race condition
  // (if swarm2 starts looking before swarm1 fully published its topics, it can miss them
  // and will retry only after quite a while)
  await rehoster.swarmInterface.swarm.flush()

  await rerehoster.add(rehoster.ownKey)

  // Connecting and updating runs in the background, so we need to give some time
  // (add more time if the change doesn't propagate in time)
  await new Promise((resolve) => setTimeout(resolve, 2000))

  console.log('rerehoster served discovery keys:')
  console.log(rerehoster.servedDiscoveryKeys) // 3 keys: its own, the rehoster's and what that one hosts

  console.log('\nThe rehoster downloads any new blocks added to the hypercore')
  someCore.on('append', () => console.log('Appended to local core--new length:', someCore.length))

  const readcore = corestore2.get({ key: someCore.key })
  await readcore.ready()
  readcore.on('append', onAppend)

  await someCore.append('A change')

  async function onAppend () {
    console.log('Change reflected in remote core--new length:', readcore.length)

    console.log('\nYou can also remove a core from the rehoster')
    await rehoster.delete(someCore.key)

    await new Promise((resolve) => setTimeout(resolve, 3000)) // Give some time to update
    console.log('rehoster:', rehoster.servedDiscoveryKeys) // Should only be 1
    console.log('rerehoster:', rerehoster.servedDiscoveryKeys) // Should only be 2

    await Promise.all([rehoster.close(), rerehoster.close()])
  }
}

main()
